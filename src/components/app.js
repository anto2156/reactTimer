import React, { Component } from 'react';
import Time from './time';
import RaisedButton from 'material-ui/RaisedButton';

export default class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      time: 0,
      circleTime: 0
    }

    this.running = false;
    this.counter = this.counter.bind(this)
    this.startTimer = this.startTimer.bind(this)
    this.pauseTimer = this.pauseTimer.bind(this)
    this.restartTimer = this.restartTimer.bind(this)
  }

  counter(){
    this.interval = setInterval(() => {
      if(this.state.time % 600 === 0){
        this.setState({
          time: this.state.time += 1,
          circleTime: 0
        })
      }else{
        this.setState({
          time: this.state.time += 1,
          circleTime: this.state.circleTime += 1
        })
      }
    }, 100)
  }

  startTimer(){
    if(this.running === false){
      this.running = true;
      this.counter();
    }
  }

  pauseTimer(){
    this.running = false;
    clearInterval(this.interval)
  }

  restartTimer(){
    this.running = false;
    clearInterval(this.interval)
    this.setState({
        time: 0,
        circleTime: 0
      })
    this.startTimer();  
  }

  render() {
    return (
      <div className="">
        < Time time={this.state.time} circleTime={this.state.circleTime} />
        <div className="button-div">
          <RaisedButton className="buttons start-button"  onClick={this.startTimer}>Start</ RaisedButton>
          <RaisedButton className="buttons pause-button"  onClick={this.pauseTimer}>Pause</ RaisedButton>
          <RaisedButton className="buttons restar-button"  onClick={this.restartTimer}>Restart</ RaisedButton>
        </div>
      </div>
    );
  }
}
