import React, { Component } from 'react';
import moment from 'moment';
import CircularProgress from 'material-ui/CircularProgress';

export default class Time extends Component{

    convertTime(){
        let ms = this.props.time;
        let seconds, minutes, hours;
        seconds = ms / 10;
        hours = parseInt( seconds / 3600 );
        seconds = seconds % 3600;
        minutes = parseInt( seconds / 60 );
        seconds = (seconds % 60).toFixed(1);
        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes; 
        seconds = (seconds < 10) ? "0" + seconds: seconds;

        return (
                <h1 className="timer">
                    {hours+":"+minutes+":"+seconds}
                </h1>
            )
    }

    render(){
        return(
            <div>
                <CircularProgress
                    className="circle-progress"
                    mode="determinate"
                    value={ this.props.circleTime }
                    size={500}
                    thickness={15}
                    max={600}
                    color={'#FFF'}
                    />  
                {this.convertTime()}
            </div>
        )
    }
    
}



                // <CircularProgress
                //     className="circle-progress-bg"
                //     mode="determinate"
                //     value={100}
                //     size={600}
                //     thickness={10}
                //     color={'#CCCCCC'}
                //     />  
